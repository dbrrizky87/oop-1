<?php


trait Hewan {
    public  $nama,
            $darah,
            $jumlahKaki,
            $keahlian;

            

            public function __construct($nama="nama", $darah=50, $jmlKaki="jmlKaki",$keahlian="keahlian", $attackPower="attack", $defencePower="defence")
            {
                $this->nama = $nama;
                $this->darah = $darah;
                $this->jmlKaki = $jmlKaki;
                $this->keahlian = $keahlian;
                $this->attackPower = $attackPower;
                $this->defencePower = $defencePower;
                }
           
            public function atraksi(){
                return " {$this->nama} sedang {$this->keahlian}" ;
            }
    
}

trait Fight 
{
    public  $attackPower, 
            $defencePower;

    use Hewan;

    public function serang($harimau) 
    {   
        return " {$this->nama} sedang menyerang {$harimau->nama}" ;
    }

    public function diserang($harimau) 
    {
        $sisaDarah = $this->darah  -  $harimau->attackPower/$this->defencePower;

        return " {$this->nama} sedang diserang {$harimau->nama} darah tersisa {$sisaDarah} " ;
    }

}


class Elang {
    use Hewan, Fight;

    public function getInfoHewan() 
    {
        $str =" Nama: {$this->nama} | Darah: {$this->darah} | Jumlah Kaki: {$this->jmlKaki} | Keahlian: {$this->keahlian} | Serangan: {$this->attackPower} | Pertahanan: {$this->defencePower} ";
      
    return $str; 

    }



}



class Harimau {
    use Hewan, Fight;

    public function getInfoHewan() 
    {
        $str =" Nama: {$this->nama} | Darah: {$this->darah} | Jumlah Kaki: {$this->jmlKaki} | Keahlian: {$this->keahlian} | Serangan: {$this->attackPower} | Pertahanan: {$this->defencePower} ";
      
    return $str; 

    }
   

}

$elang = new Elang ("Elang Jawa",50,2,"terbang tinggi", 10 , 5);
$harimau= new Harimau ("Harimau Sumatra",50,4,"lari cepat", 7, 8);
echo "Nama: {$elang->nama} | Darah: {$elang->darah} | Jumlah Kaki: {$elang->jmlKaki} | Keahlian: {$elang->keahlian} | dengan Attack Power: {$elang->attackPower} | dan Defence Power: {$elang->defencePower} ";
echo "<br>";
echo "Nama: {$harimau->nama} | Darah: {$harimau->darah} | Jumlah Kaki: {$harimau->jmlKaki} | Keahlian: {$harimau->keahlian} | dengan Attack Power: {$harimau->attackPower} | dan Defence Power: {$harimau->defencePower}";
echo "<br><br>";

echo $elang->atraksi();
echo "<br>";
echo $harimau->atraksi();
echo "<br><br>";

echo $elang->serang($harimau);
echo "<br>";
echo $harimau->serang($elang);
echo "<br><br>";

echo $elang->diserang($harimau);
echo "<br>";
echo $harimau->diserang($elang);
echo "<br><br>";

echo $elang->getInfoHewan();
echo "<br>";
echo $harimau->getInfoHewan();
echo "<br><br>";



